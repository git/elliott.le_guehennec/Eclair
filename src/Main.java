import sweet.Boulangerie;
import sweet.Client;
import sweet.Patissier;
import world.ThreadWeaver;

public class Main {
    public static void main(String[] args) {
        ThreadWeaver tw = new ThreadWeaver();
        Boulangerie b = new Boulangerie();
        Patissier p = new Patissier(b);
        Client c1 = new Client(b);
        Client c2 = new Client(b);
        tw.addRunners(p, c1, c2);
        tw.weave();
        tw.run();

        try{
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        p.shouldRun.set(false);
        tw.recover(100L);
    }
}
