import sweet.*;
import world.ThreadWeaver;

public class Main3B {
    public static void main(String[] args) {
        ThreadWeaver tw = new ThreadWeaver();
        Boulangerie b = new BoulangerieThreadSafe();
        Patissier p = new Patissier(b);
        Client c1 = new Client(b);
        tw.addRunners(p, c1);
        tw.weave();
        tw.run();

        try{
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        for(int i = 0; i < 900; i++)
            b.depose(Gateau.GATEAU_EMPOISONNE);
        tw.recover();
    }
}