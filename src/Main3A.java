import sweet.*;
import world.ThreadWeaver;

public class Main3A {
    public static void main(String[] args) {
        ThreadWeaver tw = new ThreadWeaver();
        Boulangerie b = new BoulangerieThreadSafe();
        Patissier p = new Patissier(b);
        tw.addRunners(p, new LimitedClient(b));
        tw.weave();
        tw.run();

        try{
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        p.shouldRun.set(false);
        tw.recover();
    }
}