package world;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThreadWeaver {
    private final List<Runnable> runners = new ArrayList<Runnable>();
    private final List<Thread> managed = new ArrayList<Thread>();
    public void addRunners(Runnable... addedRunners){
        runners.addAll(Arrays.stream(addedRunners).toList());
    }

    public void weave(){
        for(Runnable r: runners){
            managed.add(new Thread(r));
        }
    }

    public void run(){
        for(Thread t : managed){
            t.start();
        }
    }

    public void recover(){
        for(Thread t : managed){
            try {
                t.join();
            }catch(InterruptedException ie){
                System.out.println(ie.getMessage());
            }
        }
        termina();
    }

    public void recover(Long timeout){
        for(Thread t : managed){
            try {
                t.join(timeout);
            }catch(InterruptedException ie){
                System.out.println(ie.getMessage());
            }
        }
        termina();
    }

    public void termina(){
        for(Thread t : managed){
            if(t.isAlive()) {
                System.out.println("Thread " + t.getName() + " has not stopped being cleaned up");
                t.interrupt();
            }
        }
        managed.clear();
        runners.clear();
    }
}
