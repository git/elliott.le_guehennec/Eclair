import sweet.Boulangerie;
import sweet.BoulangerieThreadSafe;
import sweet.Client;
import sweet.Patissier;
import world.ThreadWeaver;

public class Main2 {
    public static void main(String[] args) {
        ThreadWeaver tw = new ThreadWeaver();
        Boulangerie b = new BoulangerieThreadSafe();
        Patissier p = new Patissier(b);
        tw.addRunners(p, new Client(b));
        tw.weave();
        tw.run();

        try{
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        p.shouldRun.set(false);
        tw.recover(100L);
    }
}