package sweet;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class BoulangerieThreadSafe extends Boulangerie{
    private final BlockingQueue<Patisserie> sweets = new ArrayBlockingQueue<Patisserie>(10);

    @Override
    public boolean depose(Patisserie p) {
        try{
            if(sweets.remainingCapacity() <= 0) return false;
            sweets.put(p);
        }catch(InterruptedException ignored){return false;}
        return true;
    }

    @Override
    public Patisserie achete() {
        try{return sweets.take();} catch (InterruptedException ignored) {}
        return null;
    }

    @Override
    public int getStock() {
        return sweets.size();
    }
}
