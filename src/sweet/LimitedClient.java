package sweet;

public class LimitedClient extends Client{
    private int consumed = 0;
    public LimitedClient(Boulangerie b) {
        super(b);
    }

    @Override
    public void run() {
        while(true) {
            local.achete();
            consumed++;
            if(consumed > 10) break;
            System.out.println("J'ai acheté ma patisserie - Limited");
            try {
                Thread.sleep(80);
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}
