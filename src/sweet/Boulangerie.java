package sweet;
import java.util.*;

/**
 * 
 */
public class Boulangerie {
    private final ArrayList<Patisserie> sweets = new ArrayList<>();

    public synchronized boolean depose(Patisserie p) {
        this.notify();
        sweets.add(p);
        return true;
    }


    public synchronized Patisserie achete() {
        while(getStock() == 0) {
            try {this.wait();} catch (InterruptedException ignored) {}}
            Patisserie pat = sweets.get(0);
            sweets.remove(pat);
            return pat;
    }

    public int getStock(){
        return sweets.size();
    }

}