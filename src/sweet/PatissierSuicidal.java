package sweet;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 
 */
public class PatissierSuicidal extends Patissier {
    public AtomicBoolean shouldRun = new AtomicBoolean(true);
    /**
     * Default constructor
     */
    public PatissierSuicidal(Boulangerie b) {
        super(b);
    }

    @Override
    public void run() {
        for(int i = 0; i < 20; i++) {
            if(!local.depose(new Patisserie())){
                shouldRun.set(false);
            }
            System.out.println("J'ai produit ma patisserie");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}