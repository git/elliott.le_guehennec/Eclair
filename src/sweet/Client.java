package sweet;
import java.util.*;
/**
 * 
 */
public class Client implements Runnable {
    protected final Boulangerie local;

    /**
     * Default constructor
     */
    public Client(Boulangerie b) {
        local = b;
    }

    @Override
    public void run() {
        while(local.getStock() < 9999) {
            if(local.achete() == Gateau.GATEAU_EMPOISONNE) break;
            System.out.println("J'ai acheté ma patisserie");
            try {
                Thread.sleep(80);
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}