package sweet;
import world.ThreadWeaver;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 
 */
public class Patissier implements Runnable {
    public AtomicBoolean shouldRun = new AtomicBoolean(true);
    protected final Boulangerie local;
    /**
     * Default constructor
     */
    public Patissier(Boulangerie b) {
        local = b;
    }

    @Override
    public void run() {
        while(shouldRun.get()) {
            if(!local.depose(new Patisserie())){
                shouldRun.set(false);
            }
            System.out.println("J'ai produit ma patisserie");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}